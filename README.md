# 디자인패턴 학습용 API
# 2019.01.15

# API 헤더 구조
- Headers

deviceId	:	Device Unique ID	
osType		:	OS 타입
osVersion	:	OS 버전	
appVersion	:	Application Version	앱 버전	
brand		:	Device 브랜드명	
modelNo		:	Device 모델명	
pushKey		:	Push Key	
apiToken	:	API Token 필요 시 추가
apiKey		:	API Key 필요 시 추가


# API 메시지 구조
- Request

header		:	사용자 Device 정보
parameters	:	요청 파라미터	
body		:	전송할 개체	
reqTime		:	요청한 시간	Application 에서 설정
reqRcvTime	:	요청을 접수한 시간	서버에서 설정
		
- Response

header		:	응답 상태 정보	
body		:	전송할 개체	
resTime		:	응답한 시간	서버에서 설정
resRcvTime	:	응답을 접수한 시간	Application 에서 설정

{  
   "body":{  

   },
   "header":{  
      "deviceId":"11:22:33:B4:55:6D",
      "osType":"Android",
      "osVersion":"5.0",
      "appVersion":{  
         "strVersion":"0.0.1",
         "version":[  
            "0",
            "1",
            "1"
         ]
      },
      "brand":"Samsung",
      "modelNo":"SM-G900K",
  	  "pushKey":""
      "apiKey":"",
      "apiToken":"",
   },
   "parameters":{  
      
   },
   "reqTime":"20190115001234"
}


# API 응답 헤더
errorCode		:	에러코드
errorMessage	:	에러메시지
	
{  
   "header":{  
      "errorCode":"000000",
      "errorMessage":"success"
   },
   "body": 응답결과
}


# 에러코드 정의
package com.exchangerate.demo.common.exception.APIException
해당 경로의 클래스에서 정의
