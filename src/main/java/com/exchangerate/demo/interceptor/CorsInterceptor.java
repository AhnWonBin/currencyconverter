package com.exchangerate.demo.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * 
 * @date 2019.01.15
 * @author WonBin
 * @description
 * CORS 를 위한 인터셉터, Origin 헤더가 request 에 존재할 경우, CORS 가능하도록 응답 헤더를 추가한다.
 * 
 */
@ControllerAdvice
public class CorsInterceptor implements HandlerInterceptor {
	private static final String	ACCESS_CONTROL_ALLOW_ORIGIN		 = "Access-Control-Allow-Origin";
	private static final String	ACCESS_CONTROL_ALLOW_CREDENTIALS = "Access-Control-Allow-Credentials";
//	private static final String	REQUEST_HEADER_ORIGIN			 = "Origin";
	private static final String	REQUEST_HEADER_ORIGIN			 = "*";

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		String origin = request.getHeader(REQUEST_HEADER_ORIGIN);

		// CORS 가능하도록 응답 헤더 추가
		if (StringUtils.hasLength(origin)) {
			// 요청한 도메인에 대해 CORS를 허용한다. 제한이 필요하다면 필요한 값으로 설정한다.
//			response.setHeader(ACCESS_CONTROL_ALLOW_ORIGIN, origin);

			response.setHeader("Access-Control-Allow-Origin", "*");
			response.setHeader("Access-Control-Allow-Methods", "*");
			response.setHeader("Access-Control-Max-Age", "*");
			response.setHeader("Access-Control-Allow-Headers", "*");

			// credentials 허용
			response.setHeader(ACCESS_CONTROL_ALLOW_CREDENTIALS, "true");
		}

		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		// do nothing
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		// do nothing
	}
}
