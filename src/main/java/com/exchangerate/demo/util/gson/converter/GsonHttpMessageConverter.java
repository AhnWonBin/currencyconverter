package com.exchangerate.demo.util.gson.converter;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.nio.charset.Charset;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

import com.exchangerate.demo.common.api.APIResponse;
import com.exchangerate.demo.vo.BaseVO;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

/**
 * 
 * @date 2019.01.14
 * @author WonBin
 * @description 
 * HTTP 요청, 응답 중 {@link org.springframework.http.MediaType#APPLICATION_JSON} 
 * JSON 형태의 메시지를 객체로 변환하거나 객체를 JSON 메시지로 변환한다. 
 * 요청을 객체로 변환하거나 응답을 JSON으로 변환하는 경우 모두 사용 가능하며 
 * {@link java.nio.charset.Charset} UTF-8을 처리할 수 있다.
 * 
 */
public class GsonHttpMessageConverter extends AbstractHttpMessageConverter<Object> {

	protected static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");
	protected static final String PREFIX_JSON = "{} && ";

	/**
	 * {@link GsonBuilder} Google JSON 변환기 빌더
	 */
	private GsonBuilder gsonBuilder;

	/**
	 * JSON prefix applicated if needed
	 */
	private boolean prefixJson;

	private static final ObjectMapper mapper = new ObjectMapper();

	/**
	 * Default Constructor. 기본값은 JSON Prefix 적용 안함, Null값 Serialization 적용 안함
	 */
	public GsonHttpMessageConverter() {
		this(false, false);
	}

	public GsonHttpMessageConverter(boolean serializaNulls) {
		this(false, serializaNulls);
	}

	/**
	 *
	 * @param prefixJson     JSON prefix applicated if needed, 필요한 경우 prefix 적용
	 * @param serializeNulls null값 직렬화 적용 여부
	 */
	public GsonHttpMessageConverter(boolean prefixJson, boolean serializeNulls) {
		super(new MediaType("application", "json", DEFAULT_CHARSET));
		this.gsonBuilder = GsonBuilderFactory.createGsonBuilder(false);
	}

	/**
	 *
	 * @param prefixJson     JSON prefix applicated if needed
	 * @param serializeNulls null값 직렬화 적용 여부
	 */
	public GsonHttpMessageConverter(boolean prefixJson, boolean serializeNulls, GsonBuilder gsonBuilder) {
		super(new MediaType("application", "json", DEFAULT_CHARSET));
		this.prefixJson = prefixJson;
		this.gsonBuilder = gsonBuilder;
	}

	@Override
	protected boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	protected Object readInternal(Class<?> clazz, HttpInputMessage inputMessage)
			throws IOException, HttpMessageNotReadableException {
		Reader json = null;

		try {
			json = new InputStreamReader(inputMessage.getBody(), getCharset(inputMessage.getHeaders()));
			Object rtn = gsonBuilder.create().fromJson(json, clazz);
			return rtn;
		} catch (JsonSyntaxException ex) {
			logger.info("JsonSyntaxException --->");
			logger.info(
					"[START]============================================================================================");
			logger.info(ex.getMessage());
			logger.info(
					"[END]==============================================================================================");
			throw ex;
		} catch (JsonIOException ex) {
			logger.info("JsonSyntaxException --->");
			logger.info(
					"[START]============================================================================================");
			logger.info(ex.getMessage());
			logger.info(
					"[END]==============================================================================================");
			throw ex;
		} catch (IOException ex) {
			logger.info("IOException -->");
			throw ex;
		} catch (Exception ex) {
			logger.info("Exception occurred... input string --->");
			logger.info(
					"[START]============================================================================================");
			logger.info(ex.getMessage());
			logger.info(
					"[END]==============================================================================================");
			throw ex;
		} finally {
			if (json != null) {
				json.close();
			}
		}
	}

	@Override
	protected void writeInternal(Object o, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
		OutputStreamWriter writer = null;
		Object src;

		try {
			writer = new OutputStreamWriter(outputMessage.getBody(), getCharset(outputMessage.getHeaders()));
			if (this.prefixJson) {
				writer.append(PREFIX_JSON);
			}

			if (o == null) {
				APIResponse<BaseVO> response = new APIResponse<>();
				response.setBody(null);
				src = response;
			} else {
				if (!(o instanceof APIResponse)) {
					APIResponse<Object> response = new APIResponse<>();
					response.setBody(o);
					src = response;
				} else {
					src = o;
				}
			}

			gsonBuilder.create().toJson(src, writer);

		} catch (JsonIOException ex) {
			logger.info("JsonSyntaxException --->");
			logger.info("[START]============================================================================================");
			logger.info(ex.getMessage());
			logger.info("[END]==============================================================================================");
			throw ex;
		} catch (IOException ex) {
			logger.info("IOException -->");
			logger.info("[START]============================================================================================");
			logger.info(ex.getMessage());
			logger.info("[END]==============================================================================================");
			throw ex;
		} catch (Exception ex) {
			logger.info("Exception occurred... input string --->");
			logger.info("[START]============================================================================================");
			logger.info(ex.getMessage());
			logger.info("[END]==============================================================================================");
			throw ex;
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (Exception e) {
					
				}
			}
		}
	}

	/**
	 * {@link org.springframework.http.HttpHeaders} 내에 존재하는 캐릭터셋을 반환한다.
	 *
	 * @param headers {@link org.springframework.http.HttpHeaders} HTTP 헤더
	 * @return {@link java.nio.charset.Charset} 캐릭터셋
	 */
	private Charset getCharset(HttpHeaders headers) {
		if (headers != null && headers.getContentType() != null && headers.getContentType().getCharset() != null) {
			return headers.getContentType().getCharset();
		}
		return DEFAULT_CHARSET;
	}
}
