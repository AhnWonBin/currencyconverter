package com.exchangerate.demo.util.gson.adapter;

import java.io.IOException;

import com.exchangerate.demo.util.domain.EncryptedStringVO;
import com.exchangerate.demo.util.domain.EncryptionHelper;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

/**
 * @date 2019.01.12
 * @author WonBin
 * @description 객체 {@link EncryptedStringVO}과 JSON 간의 상호 변환을 지원
 *
 */
public class EncryptedStringTypeAdapter extends TypeAdapter<EncryptedStringVO> {

	private EncryptionHelper encryptor;

	public EncryptedStringTypeAdapter() {}

	public EncryptedStringTypeAdapter(EncryptionHelper encryptor) {
		this.encryptor = encryptor;
	}

	/**
	 * 객체 {@link EncryptedStringVO}를 JSON으로 변환한다
	 *
	 * @param out {@link JsonWriter} JSON Writer
	 * @param value {@link EncryptedStringVO} 변환 대상 도메인 객체
	 * @throws IOException exception
	 */
	@Override
	public void write(JsonWriter out, EncryptedStringVO value) throws IOException {
		if (value == null) {
			out.nullValue();
			return;
		}
		if (encryptor != null) {
			out.value(encryptor.decryptTwoWay(value.getValue()));
		} else {
			out.value(value.getValue());
		}
	}

	/**
	 * JSON 문자열을 객체 {@link EncryptedStringVO}으로 변환한다
	 *
	 * @param in {@link JsonReader} JSON Reader
	 * @return {@link EncryptedStringVO} 변환된 객체
	 * @throws IOException exception
	 */
	@Override
	public EncryptedStringVO read(JsonReader in) throws IOException {
		String value = "";
		if (JsonToken.STRING.equals(in.peek())) {
			value = in.nextString();
		} else {
			throw new IOException("Unexpected json token " + in.peek().name());
		}

		if (encryptor != null) {
			return new EncryptedStringVO(encryptor.encryptTwoWay(value));
		} else {
			return new EncryptedStringVO(value);
		}
	}
}
