package com.exchangerate.demo.util.gson.strategy;

import com.google.gson.ExclusionStrategy;

/**
 * @date 2019.01.13
 * @author WonBin
 * @description Deserialization Exclusion strategy interface
 * 
 */
public interface ExclusionFromDeserializationStrategy extends ExclusionStrategy {

}
