package com.exchangerate.demo.util.gson.converter;

import com.exchangerate.demo.util.domain.EncryptedStringVO;
import com.exchangerate.demo.util.gson.adapter.EncryptedStringTypeAdapter;
import com.exchangerate.demo.util.gson.strategy.AnnotationBasedExclusionFromDeserializationStrategy;
import com.exchangerate.demo.util.gson.strategy.AnnotationBasedExclusionFromSerializationStrategy;
import com.google.gson.GsonBuilder;

/**
 * @date 2019.01.13
 * @author WonBin
 * @description Gson Converter
 *
 */
public class GsonBuilderFactory {

	private static final boolean	 DEFAULT_SERIALIZE_NULLS = false;
	private static final GsonBuilder GSON_BUILDER			 = createGsonBuilder();
	
	public static GsonBuilder defaultGsonBuilder() {
		return GSON_BUILDER;
	}

	public static GsonBuilder createGsonBuilder() {
		return createGsonBuilder(DEFAULT_SERIALIZE_NULLS);
	}

	/**
	 * @param serializeNulls true if serialize nulls
	 * @return {@link com.google.gson.GsonBuilder} Gson Builder
	 */
	public static GsonBuilder createGsonBuilder(boolean serializeNulls) {

		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setLenient();
		gsonBuilder.addSerializationExclusionStrategy(new AnnotationBasedExclusionFromSerializationStrategy())
				.addDeserializationExclusionStrategy(new AnnotationBasedExclusionFromDeserializationStrategy()).setDateFormat("yyyyMMddHHmmss")
				.registerTypeAdapter(EncryptedStringVO.class, new EncryptedStringTypeAdapter());

		if (serializeNulls) {
			gsonBuilder.serializeNulls();
		}

		return gsonBuilder;
	}

	/**
	 * Return GsonBuilder when using apps No strategy (strategy 미적용)
	 *
	 * @param serializeNulls true if serialize nulls
	 * @return {@link com.google.gson.GsonBuilder} Gson Builder
	 */
	public static GsonBuilder createGsonBuilderWithoutStrategy(boolean serializeNulls) {

		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setLenient();
		gsonBuilder.setDateFormat("yyyyMMddHHmmss").registerTypeAdapter(EncryptedStringVO.class, new EncryptedStringTypeAdapter());

		if (serializeNulls) {
			gsonBuilder.serializeNulls();
		}

		return gsonBuilder;
	}
}
