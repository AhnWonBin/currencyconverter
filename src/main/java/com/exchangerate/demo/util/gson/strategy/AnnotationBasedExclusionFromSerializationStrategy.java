package com.exchangerate.demo.util.gson.strategy;

import com.exchangerate.demo.util.domain.ExcludeFromSerialization;
import com.google.gson.FieldAttributes;

/**
 * @date 2019.01.13
 * @author WonBin
 * @description 
 * {@link ExcludeFromSerialization} 
 * annotation으로 태깅된 필드는 Serialization, Deserialization에서 제외한다.
 */
public class AnnotationBasedExclusionFromSerializationStrategy implements ExclusionFromSerializationStrategy {

    /**
     * {@link ExcludeFromSerialization} annotation으로 태깅된 필드는 제외한다.
     *
     * @param fieldAttributes {@link FieldAttributes}
     * @return if the field tagged with {@link ExcludeFromSerialization} annotation
     */
    @Override
    public boolean shouldSkipField(FieldAttributes fieldAttributes) {
        return fieldAttributes.getAnnotation(ExcludeFromSerialization.class) != null;
    }

    /**
     * 이 strategy에서 클래스는 제외되지 않는다.
     *
     * @param clazz class to skip
     * @return always <pre>false</pre>
     */
    @Override
    public boolean shouldSkipClass(Class<?> clazz) {
        return false;
    }
}
