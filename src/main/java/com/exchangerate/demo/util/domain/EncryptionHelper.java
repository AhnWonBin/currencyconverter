package com.exchangerate.demo.util.domain;

/**
 * @date 2019.01.12
 * @author WonBin
 *
 */
public interface EncryptionHelper {
	/**
	 * 
	 * @param plainText (일반 텍스트)
	 * @return
	 */
	String encryptTwoWay(String plainText);
	/**
	 * 
	 * @param cipherText (암호 텍스트)
	 * @return
	 */
	String decryptTwoWay(String cipherText);
	
	String encryptOneWay(String plainText);
	
	/**
	 * 
	 * @param plainText (일반 텍스트)
	 * @param encryptedText (암호화된 텍스트)
	 * @return
	 */
	boolean verifyOneWay(String plainText, String encryptedText);
	
}
