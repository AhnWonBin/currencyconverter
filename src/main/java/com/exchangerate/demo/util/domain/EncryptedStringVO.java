package com.exchangerate.demo.util.domain;

import com.exchangerate.demo.vo.BaseVO;

/**
 * @date 2019.01.12
 * @author WonBin
 * @description 
 * EncryptVO 필요시 사용
 *
 */
public class EncryptedStringVO extends BaseVO {
	
	private static final long serialVersionUID = 7133491063412444597L;
	
	protected static EncryptionHelper enctyptionHelper;

	protected String value;

	public EncryptedStringVO() {}

	/**
	 * @param value (the origin value)
	 */
	public EncryptedStringVO(String value) {
		this.value = value;
	}

	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * 암호 해제 to Decrypted value
	 * @return String
	 */
	public String toDecryptedValue() {
		return value;
	}

	/**
	 * 암호화 from Decrypted value
	 * @param value 
	 */
	public void fromDecryptedValue(String value) {
		this.value = value;
	}

	/**
	 * decrypted
	 * @return 암호화 문자열
	 */
	public String decrypte() {
		if (enctyptionHelper == null) throw new RuntimeException("Did not set enctyptionHelper.");
		return EncryptedStringVO.enctyptionHelper.decryptTwoWay(this.value);
	}

	/**
	 * setEncryptor
	 * @param enctyptionHelper 암호화 핸들러
	 */
	public static void setEncryptor(EncryptionHelper enctyptionHelper) {
		EncryptedStringVO.enctyptionHelper = enctyptionHelper;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof EncryptedStringVO))
			return false;
		EncryptedStringVO other = (EncryptedStringVO) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
}
