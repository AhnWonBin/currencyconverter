package com.exchangerate.demo.util;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;

import com.exchangerate.demo.base.BaseSpringApplication;

@Configuration
@PropertySource("config.properties")
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class ServerUtil {
	private static final Logger logger = LoggerFactory.getLogger(ServerUtil.class);
	
	public static final String CURRENCYLAYER_API_URL = "currencylayer.api.url";
	public static final String CURRENCYLAYER_API_ACCESSKEY = "currencylayer.api.accesskey";
	
	private static Environment environment;
	
	public static Environment getEnvironment() {
		if(environment == null) {
			environment = BaseSpringApplication.getApplicationContext().getBean(Environment.class);
		}
		return environment;
	}

}
