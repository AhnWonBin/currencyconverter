package com.exchangerate.demo.util;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public abstract class StringUtil {

	public static boolean isEmpty(String str) {
		return str == null || str.equals("") || str.equals("null");
	}

	public static boolean isNotEmpty(String str) {
		return !isEmpty(str);
	}

	/**
	 * int 천 단위로 콤마
	 * 
	 * @param int
	 * @return String
	 */
	public static String toNumFormat(int num) {
		DecimalFormat decimalFormat = new DecimalFormat("#,###");
		return decimalFormat.format(num);
	}

	public static String toNumFormat(double num) {
		NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setMaximumFractionDigits(2);
		numberFormat.setRoundingMode(RoundingMode.DOWN);
		numberFormat.setGroupingUsed(true);

		return numberFormat.format(num);
	}

	/**
	 * String 천 단위로 콤마, 소수점 3째 자리에서 반올림
	 * 
	 * @param String
	 * @return String
	 */
	public static String toNumFormat(String num) {
		double parseValue = Double.parseDouble(num);
		NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setMaximumFractionDigits(2);
		numberFormat.setRoundingMode(RoundingMode.DOWN);
		numberFormat.setGroupingUsed(true);

		return numberFormat.format(parseValue);
	}

	/**
	 * 문자열이 null인경우 replace_str을 Return한다. 사용 예) 테이블의 &lt;td&gt;str&lt;/td&gt; 에서
	 * str이 null인 경우 replate_str이 &nbsp;로 지정한다.
	 *
	 * @param str         Null 문자열
	 * @param replace_str 변환할 문자열
	 * @return 변환할 문자열
	 */
	static public String NVL(String str, String replace_str) {
		if (StringUtil.isEmpty(str)) {
			return replace_str;
		} else {
			return str;
		}
	}

	/**
	 * String s에 있는 알파벳을 모두 소문자로 바꾸어 return
	 *
	 * @param s source String
	 */
	public static String toLowerCase(String s) {
		if (StringUtil.isEmpty(s)) {
			return null;
		}

		int i;
		int j;
		char c;
		label0: {
			i = s.length();
			for (j = 0; j < i; j++) {
				char c1 = s.charAt(j);
				c = Character.toLowerCase(c1);
				if (c1 != c)
					break label0;
			}

			return s;
		}
		char ac[] = new char[i];
		int k;
		for (k = 0; k < j; k++)
			ac[k] = s.charAt(k);

		ac[k++] = c;
		for (; k < i; k++)
			ac[k] = Character.toLowerCase(s.charAt(k));

		String s1 = new String(ac, 0, i);
		return s1;
	}

	/**
	 * String s에 있는 alphabet을 모두 대문자로 바꾸어 return
	 *
	 * @param s source String
	 */
	public static String toUpperCase(String s) {
		if (StringUtil.isEmpty(s)) {
			return null;
		}

		int i;
		int j;
		char c;
		label0: {
			i = s.length();
			for (j = 0; j < i; j++) {
				char c1 = s.charAt(j);
				c = Character.toUpperCase(c1);
				if (c1 != c)
					break label0;
			}

			return s;
		}
		char ac[] = new char[i];
		int k;
		for (k = 0; k < j; k++)
			ac[k] = s.charAt(k);

		ac[k++] = c;
		for (; k < i; k++)
			ac[k] = Character.toUpperCase(s.charAt(k));

		return new String(ac, 0, i);
	}

	/**
	 * Unix Time 한국시간 변환
	 * 
	 * @param time		String UnixTime
	 * @return String	변환된 시간
	 */
	public static String toUnixTime(String time) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		long unixTime = Long.parseLong(time);
		String formattedDate = Instant.ofEpochSecond(unixTime).atZone(ZoneId.of("GMT+9")).format(formatter);

		return formattedDate;
	}
	
	public static double toDouble(String str) {
		double tmp = Double.parseDouble(str);
		return tmp;
	}
	
	public static String reg(String str) {
		String reg1 = "[a-zA-Zㄱ-ㅎ가-힣]";
		String s1 = str.replaceAll(reg1, "");
		String reg2 = "[^0-9]{1}[^0-9]+";
		String s2 = s1.replaceAll(reg2, "");
		
		return s2;
	}
}
