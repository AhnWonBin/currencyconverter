package com.exchangerate.demo.service;

import java.util.Map;

import com.exchangerate.demo.dto.CurrencyDTO;

public interface CurrencyService {
	public String currencyApiCall();

	public CurrencyDTO getCallback();

	public String getCurrency(Map<String, Object> paramMap);

}
