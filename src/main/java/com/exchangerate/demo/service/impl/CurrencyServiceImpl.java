package com.exchangerate.demo.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.exchangerate.demo.dto.CurrencyDTO;
import com.exchangerate.demo.service.CurrencyService;
import com.exchangerate.demo.util.ServerUtil;
import com.exchangerate.demo.util.StringUtil;

@Service
public class CurrencyServiceImpl implements CurrencyService {

	@Value("${currencylayer.api.url}")
	String URL;

	@Value("${currencylayer.api.accesskey}")
	String KEY;

	@Value("${currencylayer.api.url2}")
	String URL2;

	@Value("${currencylayer.api.path2}")
	String PATH2;

	@Value("${currencylayer.api.accesskey2}")
	String KEY2;

	private static final Logger logger = LoggerFactory.getLogger(CurrencyServiceImpl.class);

	private final RestTemplate restTemplate;

	public CurrencyServiceImpl(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}

	@Override
	public String currencyApiCall() {
		Environment env = ServerUtil.getEnvironment();

		StringBuffer response = new StringBuffer();
		BufferedReader br = null;

		try {
			URL obj = new URL(URL + KEY);

			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");

			int responseCode = con.getResponseCode();
			// API 정상 호출
			if (responseCode == 200) {
				logger.info("정상호출 : " + responseCode);
				br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
			} else {
				logger.error("에러 : " + responseCode);
				br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
			}

			String line = null;
			while ((line = br.readLine()) != null) {
				response.append(line);
			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException ioe) {
					logger.error(ioe.getMessage(), ioe);
					ioe.printStackTrace();
				}
			}
		}

		CurrencyDTO testDto = getCallback();
		if (testDto.getSuccess()) {
			// dto
		} else {
			// error
		}

		return response.toString();
	}

	@Override
	public CurrencyDTO getCallback() {
		UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
											.scheme("http")
											.host(URL2)
											.path(PATH2)
											.queryParam("access_key", KEY2);

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

		HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);
		return restTemplate.exchange(builder.toUriString(), HttpMethod.GET, httpEntity, CurrencyDTO.class).getBody();
	}

	@Override
	public String getCurrency(Map<String, Object> paramMap) {
		CurrencyDTO dto = (CurrencyDTO) paramMap.get("dto");
		String from = (String) paramMap.get("from");
		String to = (String) paramMap.get("to");
		String fromValue = (String) paramMap.get("fromValue");
		String toValue;
		double tmp = Double.parseDouble(fromValue);
		double sum;

		switch (to) {
		case "JPY":
			sum = tmp * dto.getQuotes().getUSDJPY();
			break;
		case "KRW":
			sum = tmp * dto.getQuotes().getUSDKRW();
			break;
		case "PHP":
			sum = tmp * dto.getQuotes().getUSDPHP();
			break;
		default:
			sum = 1.00;
			break;
		}
		toValue = StringUtil.toNumFormat(sum);

		return toValue;
	}

}