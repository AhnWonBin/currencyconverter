package com.exchangerate.demo.vo.req;

import com.exchangerate.demo.common.api.BaseAPIParam;
import com.exchangerate.demo.vo.CurrencyVO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @date 2019.01.15
 * @author WonBin
 * @description API-Currency-001 Parameter
 * 
 */

@Data
@EqualsAndHashCode(callSuper = false)
public class APICurrency001Param extends BaseAPIParam {

	private static final long serialVersionUID = 1951283839567034758L;
	
	private CurrencyVO currencyVO;
	
}
