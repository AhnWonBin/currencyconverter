package com.exchangerate.demo.vo;

import java.io.Serializable;

import com.exchangerate.demo.util.gson.converter.GsonBuilderFactory;

import lombok.Data;

/**
 * @date 2019.01.14
 * @author WonBin
 * @description Value Object의 Base Class
 * 
 */

@Data
public class BaseVO implements Serializable, Cloneable {
	public BaseVO() {
		super();
	}

	// Base 객체는 변환이 잘 될 수 있음, Map에 지정된 Base객체들은 변환이 불가 추후 수정 필요
	// double형에 대한 이슈가 있음
	public Object clone() {
		String json = GsonBuilderFactory.defaultGsonBuilder().create().toJson(this);
		Object result = GsonBuilderFactory.defaultGsonBuilder().create().fromJson(json, this.getClass());
		return result;
	}
}
