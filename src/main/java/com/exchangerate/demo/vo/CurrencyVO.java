package com.exchangerate.demo.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class CurrencyVO extends BaseVO {
	
	private static final long serialVersionUID = -2058027541061595380L;
	
	private String from;
	private String to;
	private String fromValue;
	private String toValue;
}
