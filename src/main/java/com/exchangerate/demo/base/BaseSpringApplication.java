package com.exchangerate.demo.base;

import org.springframework.beans.BeansException;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

/**
 * 
 * @author WonBin
 * @description 패키지 별 컨텍스트 활용가능
 *
 */
public abstract class BaseSpringApplication {
	
	private static ApplicationContext applicationContext;
	

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public static void setApplicationContext(ApplicationContext applicationContext) {
        BaseSpringApplication.applicationContext = applicationContext;
    }

    public static ConfigurableApplicationContext startSpringApplication(Class applicationClass, String[] args) {
        ConfigurableApplicationContext configurableApplicationContext = SpringApplication.run(applicationClass, args);
        setApplicationContext(configurableApplicationContext);
        return configurableApplicationContext;
    }

    @Component
    public static class SpringApplicationContext implements ApplicationContextAware {
        @Override
        public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
            BaseSpringApplication.applicationContext = applicationContext;
        }
    }
}
