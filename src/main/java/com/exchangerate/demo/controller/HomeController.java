package com.exchangerate.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.exchangerate.demo.common.exception.APIException;
import com.exchangerate.demo.dto.CurrencyDTO;
import com.exchangerate.demo.service.CurrencyService;
import com.exchangerate.demo.util.StringUtil;

@Controller
public class HomeController {
	@Autowired
	CurrencyService currencyService;
	
	@RequestMapping(value="/", method=RequestMethod.GET)
	public ModelAndView home(ModelAndView mav) {
		CurrencyDTO dto = currencyService.getCallback();
		
		mav.setViewName("test");
		mav.addObject("message", "Currency Converter Test");
		mav.addObject("fromTo", "USD/KRW");
		mav.addObject("fromValue", "1.00");
		
		if(dto.getError() != null) {
			mav.addObject("toValue", "0.00");
			mav.addObject("msg", APIException.getErrorMessage("999001"));
		} else {
			mav.addObject("toValue", StringUtil.toNumFormat(dto.getQuotes().getUSDKRW()));
		}
		
		return mav;
	}
	
}
