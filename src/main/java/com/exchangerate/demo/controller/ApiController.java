package com.exchangerate.demo.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.exchangerate.demo.common.api.APIResponse;
import com.exchangerate.demo.common.api.APIResponseUtil;
import com.exchangerate.demo.common.exception.APIException;
import com.exchangerate.demo.common.provider.APIRequestProvider;
import com.exchangerate.demo.dto.CurrencyDTO;
import com.exchangerate.demo.service.CurrencyService;
import com.exchangerate.demo.util.StringUtil;
import com.exchangerate.demo.vo.CurrencyVO;
import com.exchangerate.demo.vo.req.APICurrency001Param;

@RestController
public class ApiController {
	
	@Autowired
	private CurrencyService currencyService;

	@SuppressWarnings("unchecked")
	@RequestMapping("exchangerates")
	public APIResponse<CurrencyVO> apiCall4(@RequestBody final APIRequestProvider.CURRENCY.APICurrency001Req currency001Req) {
		APICurrency001Param apiCurrency001Param = currency001Req.getParameters();
		
		CurrencyVO currencyVO = apiCurrency001Param.getCurrencyVO();
		
		if(currencyVO == null) {
			return APIResponseUtil.makeFailResult("999001", APIException.getErrorMessage("999001"));
		}
		
		Map<String, Object> map = new HashMap<>();
		
		String toFromValue = StringUtil.reg(currencyVO.getFromValue());
		if(toFromValue.equals("") || toFromValue == null) {
			toFromValue = "1.00";
		}
		currencyVO.setFromValue(toFromValue);
		map.put("fromTo", currencyVO.getFrom() + "/" + currencyVO.getTo());
		
		double valid = StringUtil.toDouble(currencyVO.getFromValue());
		if(valid <= 0.0 || valid > 10000.0) {
			return APIResponseUtil.makeFailResult("100001", APIException.getErrorMessage("100001"));
		}
		
		CurrencyDTO dto = currencyService.getCallback();
		
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("dto", dto);
		paramMap.put("from", currencyVO.getFrom());
		paramMap.put("to", currencyVO.getTo());
		paramMap.put("fromValue", currencyVO.getFromValue());

		if(dto.getError() != null) {
			map.put("toValue", "0.00");
			return APIResponseUtil.makeSuccessResult(map);
		}
		
		String currency = currencyService.getCurrency(paramMap);
		map.put("fromValue", currencyVO.getFromValue());
		map.put("toValue", currency);
		
		return APIResponseUtil.makeSuccessResult(map);
	}
}
