package com.exchangerate.demo.dto;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "USDPHP", "USDJPY", "USDKRW" })
public class QuotesDTO {

	@JsonProperty("USDPHP")
	private Double USDPHP;
	@JsonProperty("USDJPY")
	private Double USDJPY;
	@JsonProperty("USDKRW")
	private Double USDKRW;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public Double getUSDPHP() {
		return USDPHP;
	}

	public void setUSDPHP(Double USDPHP) {
		this.USDPHP = USDPHP;
	}

	public Double getUSDJPY() {
		return USDJPY;
	}

	public void setUSDJPY(Double USDJPY) {
		this.USDJPY = USDJPY;
	}

	public Double getUSDKRW() {
		return USDKRW;
	}

	public void setUSDKRW(Double USDKRW) {
		this.USDKRW = USDKRW;
	}

	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}

	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

}
