package com.exchangerate.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;

@SpringBootApplication
public class ExchangRateApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExchangRateApplication.class, args);
	}

}

@Configuration
class ThymeleafConfig {
	@Autowired
	ApplicationContext applicationContext;

	// Creating SpringResourceTemplateResolver
	@Bean
	public SpringResourceTemplateResolver springTemplateResolver() {
		SpringResourceTemplateResolver springTemplateResolver = new SpringResourceTemplateResolver();
		springTemplateResolver.setApplicationContext(this.applicationContext);
		springTemplateResolver.setPrefix("/WEB-INF/views/");
		springTemplateResolver.setSuffix(".html");
		springTemplateResolver.setCacheable(false);
		return springTemplateResolver;
	}

	// Creating SpringTemplateEngine
	@Bean
	public SpringTemplateEngine springTemplateEngine() {
		SpringTemplateEngine springTemplateEngine = new SpringTemplateEngine();
		springTemplateEngine.setTemplateResolver(springTemplateResolver());
		return springTemplateEngine;
	}

	// Registering ThymeleafViewResolver
	@Bean
	public ViewResolver viewResolver() {
		ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
		viewResolver.setTemplateEngine(springTemplateEngine());
		viewResolver.setCharacterEncoding("UTF-8");
		viewResolver.setOrder(1);
		return viewResolver;
	}
	
}
