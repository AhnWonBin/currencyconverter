package com.exchangerate.demo.type;

public enum OSType {
	Android("ANDROID"), iOS("iOS"), WindowMobile("WINMO"), Windows("WINDOWS"), MAC("MAC"), LINUX("LINUX"), Others("ETC");

	private String osType;

	private OSType(String osType) {
		this.osType = osType;
	}

	public String getOsType() {
		return osType;
	}

	public static OSType getEnum(String osType) {
		if (osType == null) {
			return Others;
		}
		if (OSType.Android.getOsType().equals(osType)
				|| OSType.Android.name().toLowerCase().equals(osType.toLowerCase())) {
			return OSType.Android;
		} else if (OSType.iOS.getOsType().equals(osType)
				|| OSType.iOS.name().toLowerCase().equals(osType.toLowerCase())) {
			return OSType.iOS;
		} else if (OSType.WindowMobile.getOsType().equals(osType)
				|| OSType.WindowMobile.name().toLowerCase().equals(osType.toLowerCase())) {
			return OSType.WindowMobile;
		} else if (OSType.Windows.getOsType().equals(osType)
				|| OSType.Windows.name().toLowerCase().equals(osType.toLowerCase())) {
			return OSType.Windows;
		} else if (OSType.MAC.getOsType().equals(osType)
				|| OSType.MAC.name().toLowerCase().equals(osType.toLowerCase())) {
			return OSType.MAC;
		} else if (OSType.LINUX.getOsType().equals(osType)
				|| OSType.LINUX.name().toLowerCase().equals(osType.toLowerCase())) {
			return OSType.LINUX;
		}

		return OSType.Others;
	}
}