package com.exchangerate.demo.type;

public enum SourceType {
	USD("USD"),
	KRW("KRW"),
	JPY("JPY"),
	PHP("PHP"),
	UNKNOWN("UNKNOWN");
	
	private String source;

	private SourceType(String source) {
		this.source = source;
	}

	public String getsource() {
		return source;
	}

	public static SourceType getEnum(String source) {
		if (SourceType.KRW.getsource().equals(source)) {
			return SourceType.KRW;
		} else if (SourceType.JPY.getsource().equals(source)) {
			return SourceType.JPY;
		} else if (SourceType.PHP.getsource().equals(source)) {
			return SourceType.PHP;
		}

		return SourceType.UNKNOWN;
	}
	
}
