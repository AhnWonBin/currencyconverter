package com.exchangerate.demo.common.exception;

import java.util.HashMap;
import java.util.Map;

/**
 * @date 2019.01.14
 * @author WonBin
 * @description API Exception 명세
 *
 */
public class APIException extends BaseException {
	private static final long serialVersionUID = 1L;
	
	private static Map<String, String> errorMsgMap = new HashMap<>();

	static {
		errorMsgMap.put("999999", "알 수 없는 오류가 발생했습니다.");
		
		errorMsgMap.put("999001", "요청하신 정보가 올바르지 않습니다.");
		errorMsgMap.put("999002", "요청하신 처리를 실패하였습니다.");
		
		errorMsgMap.put("100001", "송금액이 올바르지 않습니다.");
	}

	public static String getErrorMessage(String errorCode) {
		if (errorMsgMap.containsKey(errorCode)) {
			return errorMsgMap.get(errorCode);
		}
		return errorMsgMap.get("999999");
	}

	public APIException(String errorCode) {
		this.setErrorCode(errorCode);
	}

	public APIException(String errorCode, String errorMessage) {
		super(errorCode, errorMessage);
	}

	@Override
	public String getMessage() {
		return getErrorMessage(this.getErrorCode());
	}

}
