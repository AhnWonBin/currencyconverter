package com.exchangerate.demo.common.exception;

/**
 * @date 2019.01.15
 * @author WonBin
 * @description Exception 핸들링을 위한 Base Exception Class
 *
 */
public class BaseException extends Exception {
	private static final long serialVersionUID = 1L;

	private String errorCode;

	public BaseException() {
		super();
	}

	public BaseException(String errorCode) {
		super();
		this.errorCode = errorCode;
	}

	public BaseException(String errorCode, String errorMessage) {
		super(errorMessage);
		this.errorCode = errorCode;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

}
