package com.exchangerate.demo.common.provider;

import com.exchangerate.demo.common.api.APIRequest;
import com.exchangerate.demo.vo.CurrencyVO;
import com.exchangerate.demo.vo.req.*;

/**
 * @date 2019.01.15
 * @author WonBin
 * @description API Request Provider 추상화 클래스
 *
 */
public abstract class APIRequestProvider {
	
	public static class CURRENCY {
		public static class APICurrency001Req extends APIRequest<CurrencyVO, APICurrency001Param> { private static final long serialVersionUID = 1L; }
	}
	
}
