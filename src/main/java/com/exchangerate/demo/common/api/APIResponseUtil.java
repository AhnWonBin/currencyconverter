package com.exchangerate.demo.common.api;

/**
 * @date 2019.01.14
 * @author WonBin
 * @description API의 Response Return
 *
 */

public abstract class APIResponseUtil {
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static APIResponse makeSuccessResult(Object result) {
		APIResponse apiResponse = new APIResponse<>();
		apiResponse.setBody(result);
		return apiResponse;
	}
	
	@SuppressWarnings("rawtypes")
	public static APIResponse makeFailResult(String errorCode) {
		return makeFailResult(errorCode, "Error!");
	}

	@SuppressWarnings("rawtypes")
	public static APIResponse makeFailResult(String errorCode, String message) {
		APIResponse apiResponse = new APIResponse<>();
		apiResponse.getHeader().setErrorCode(errorCode);
		apiResponse.getHeader().setErrorMessage(message);
		
		return apiResponse;
	}
}
