package com.exchangerate.demo.common.api;

import com.exchangerate.demo.type.OSType;
import com.exchangerate.demo.vo.BaseVO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @date 2019.01.14
 * @author WonBin
 * @description API Request Header
 * 
 */

@Data
@EqualsAndHashCode(callSuper = false)
public class APIRequestHeader extends BaseVO {

	private static final long serialVersionUID = 5477644982451378772L;
	
	/**
	 * Device ID Unique
	 */
	protected String deviceId;
	/**
	 * OS 타입
	 */
	protected OSType osType;
	/**
	 * OS 버전
	 */
	protected String osVersion;
	/**
	 * App 버전 객체
	 */
	protected ApplicationVersion appVersion;
	/**
	 * 제조사 브랜드
	 */
	protected String brand;
	/**
	 * 모델번호
	 */
	protected String modelNo;
	/**
	 * Device Push Key
	 */
	protected String pushKey;
	/**
	 * API Token
	 */
	protected String apiToken;
	/**
	 * API 요청 Key
	 */
	protected String apiKey;

}