package com.exchangerate.demo.common.api;

import com.exchangerate.demo.vo.BaseVO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @date 2019.01.15
 * @author WonBin
 * @description Application Version Wrapping Class
 * 버전 정보 및 서버에서 가져온 버전과 비교했을 때의 처리를 boolean으로 리턴s
 *
 */

@Data
@EqualsAndHashCode(callSuper = false)
public class ApplicationVersion extends BaseVO {

	private static final long serialVersionUID = 8331107481836293184L;

	/**
	 * ApplicationVersion.
	 *
	 */
	public static enum CompareResult {
		OLDER, NEWER, EQUAL;
	};

	/**
	 * 버전 문자열. 별도로 설정하지 않아도 됨
	 */
	private String[] version;

	/**
	 * 문자열로된 App의 버전
	 */
	private String strVersion;

	public ApplicationVersion() {
	}

	/**
	 * ApplicationVersion's constructor
	 *
	 * @param appVersion
	 */
	public ApplicationVersion(String appVersion) {
		strVersion = appVersion;
		version = parseVersion(appVersion);
	}

	/**
	 * parseVersion
	 *
	 * @param version
	 * @return String[]
	 */
	private String[] parseVersion(String version) {
		return version.split("\\.");
	}

	/**
	 * compareTo
	 *
	 * @param cmp
	 * @return CompareResult
	 */
	public CompareResult compareTo(String cmp) {
		String[] cmpVer = parseVersion(cmp);
		int thisLen = version.length;
		int cmpLen = cmpVer.length;
		int shorter = thisLen > cmpLen ? cmpLen : thisLen;

		for (int i = 0; shorter > i; i++) {

			int result = version[i].compareToIgnoreCase(cmpVer[i]);

			if (result > 0) {

				return CompareResult.NEWER;

			} else if (result < 0) {

				return CompareResult.OLDER;
			}
		}

		return thisLen > cmpLen ? CompareResult.NEWER : thisLen == cmpLen ? CompareResult.EQUAL : CompareResult.OLDER;
	}

	/**
	 * isOlderThan
	 *
	 * @param cmp
	 * @return boolean
	 */
	public boolean isOlderThan(String cmp) {
		return compareTo(cmp) == CompareResult.OLDER;
	}

	/**
	 * isNewerThan
	 *
	 * @param cmp
	 * @return boolean
	 */
	public boolean isNewerThan(String cmp) {
		return compareTo(cmp) == CompareResult.NEWER;
	}

	/**
	 * isEqualTo
	 *
	 * @param cmp
	 * @return boolean
	 */
	public boolean isEqualTo(String cmp) {
		return compareTo(cmp) == CompareResult.EQUAL;
	}

}
