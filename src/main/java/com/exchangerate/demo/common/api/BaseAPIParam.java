package com.exchangerate.demo.common.api;

import com.exchangerate.demo.vo.BaseVO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @date 2019.01.14
 * @author WonBin
 * @description API Request 구성요소 중 Parameter의 Base Class
 * 
 */

@Data
@EqualsAndHashCode(callSuper = false)
public abstract class BaseAPIParam extends BaseVO {

}
