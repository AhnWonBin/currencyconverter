package com.exchangerate.demo.common.api;

import java.util.Date;

import com.exchangerate.demo.vo.BaseVO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @date 2019.01.15
 * @author WonBin
 * @description API의 Response시 전체를 Wrapping 하는 클래스
 *
 */

@Data
@EqualsAndHashCode(callSuper = false)
public class APIResponse<TBODY> extends BaseVO {
	
	private static final long serialVersionUID = 508721524638007675L;
	
	/**
	 * 응답전송 서버에서 발송직전에 세팅
	 */
	protected Date resTime = new Date();
	/**
	 * 응답 수신 Client에서 수신받자 마자 세팅
	 */
	protected Date resRcvTime;

	/**
	 * API Response Header
	 */
	APIResponseHeader header = new APIResponseHeader();

	/**
	 * 전송할 데이터 객체
	 */
	TBODY body;

	public APIResponse() {}

	public APIResponse(String errorCode, String errorMessage, String errorDetailCode, String errorDetailMessage) {
		header = new APIResponseHeader(errorCode, errorMessage, errorDetailCode, errorDetailMessage);
	}
	
}
