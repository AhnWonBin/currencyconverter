package com.exchangerate.demo.common.api;

import java.lang.reflect.ParameterizedType;
import java.util.Date;

import com.exchangerate.demo.vo.BaseVO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @date 2019.01.14
 * @author WonBin
 * @description API Request 시 전체 Wrappings
 * 
 */

public abstract class APIRequest<TBODY, TPARAM> extends BaseVO {
	
	private static final long serialVersionUID = -6023948116741411239L;
	
	/**
	 * Request Header
	 */
	protected APIRequestHeader header = new APIRequestHeader();
	/**
	 * API의 요청 API를 Network로 전송하기 전에 세팅
	 */
	protected Date reqTime;
	/**
	 * API의 수신 서버에서 전달받은 때 세팅
	 */
	protected Date reqRcvTime = new Date();
	/**
	 * 요청시 Body
	 */
	protected TBODY body;
	/**
	 * 요청시 전달 Paramaters
	 */
	protected TPARAM parameters;

	public TPARAM getParameters() {
		if (parameters == null) {
			try {
				Class<TPARAM> clazz = (Class<TPARAM>) this.getTypeArguments(1);
				parameters = clazz.newInstance();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		return parameters;
	}

	@SuppressWarnings("unchecked")
	private Class<TPARAM> getTypeArguments(int index) {
		ParameterizedType paramType = (ParameterizedType) this.getClass().getGenericSuperclass();
		return (Class<TPARAM>) paramType.getActualTypeArguments()[index];
	}
	
}