package com.exchangerate.demo.common.api;

import com.exchangerate.demo.vo.BaseVO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @date 2019.01.14
 * @author WonBin
 * @description API Response header의 정보
 * 
 */

@Data
@EqualsAndHashCode(callSuper = false)
public class APIResponseHeader extends BaseVO {

	private static final long serialVersionUID = 2780044021057119023L;
	
	public static final String SUCCESS_CODE = "000000";
	/**
	 * 오류 코드 요청 성공 : "000000" 그 외의 경우 오류 상태이며 Exception Class 정의 필요
	 */
	private String errorCode = SUCCESS_CODE;

	/**
	 * 오류 메시지(표시용), 오류 상태 경우 존재 성공 시 default = success
	 */
	private String errorMessage = "success";

	/**
	 * 오류 상세 코드, Exception Class 정의 필요
	 */
	private String errorDetailCode;

	/**
	 * 오류 상세 메시지, Exception Class 정의 필요
	 */
	private String errorDetailMessage;

	public APIResponseHeader() {
	}

	public APIResponseHeader(String errorCode, String errorMessage, String errorDetailCode, String errorDetailMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.errorDetailCode = errorDetailCode;
		this.errorDetailMessage = errorDetailMessage;
	}

}
